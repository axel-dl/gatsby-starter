import React from "react"
import { Helmet } from "react-helmet"
import PropTypes from "prop-types"
import { graphql, useStaticQuery } from "gatsby"

const SEO = ({ title, description, slug, article }) => {
    const {site} = useStaticQuery(graphql`
        query SEO {
            site {
                siteMetadata {
                    title
                    description
                    siteUrl
                    image
                }
            }
        }
    `)
    const seo = {
        title: title === undefined ? site.siteMetaData.title : title,
        description: description === undefined ? site.siteMetaData.description : description,
        image: `${site.siteUrl}${site.siteMetadata.image}`,
        url: `${site.siteUrl}${slug === undefined ? "/" : slug}`,
    }
    return (
        <Helmet title={seo.title} titleTemplate={title}>
            <meta name="description" content={seo.description} />
            <meta name="image" content={seo.image} />
            {seo.url && (
                <meta property="og:url" content={seo.url} />
            )}
            {(article ? true : null) && (
                <meta property="og:type" content="article" />
            )}
            {seo.title && (
                <meta property="og:title" content={seo.title} />
            )}
            {seo.description && (
                <meta
                    property="og:description"
                    content={seo.description}
                />
            )}
            {seo.image && (
                <meta property="og:image" content={seo.image} />
            )}
            {seo.title && (
                <meta name="twitter:title" content={seo.title} />
            )}
        </Helmet>
    )
}

export default SEO

SEO.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    image: PropTypes.string,
    pathname: PropTypes.string,
    article: PropTypes.bool,
}

SEO.defaultProps = {
    title: null,
    description: null,
    image: null,
    pathname: null,
    article: false,
}
