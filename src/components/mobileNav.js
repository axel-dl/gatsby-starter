import React, { useState, useEffect } from "react"
// import Link from "gatsby-plugin-transition-link"
import { Link } from "gatsby"
import Wrapper from "../components/util/wrapper"
import styled, { css }from "styled-components"
import { useMainMenu } from "../hooks/useMainMenu"
import {
    disableBodyScroll,
    enableBodyScroll,
    clearAllBodyScrollLocks,
} from "body-scroll-lock"


const MobileNavWrapper = styled.div`
    position: fixed;
    ${props => `height: calc(100vh - ${props.headerHeight}px)`};
    width: 100vw;
    background-color: green;
`

const MobileNavItem = styled(Link)``

const MobileNavInnerWrap = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    height: 100%;
`
const StyledWrapper = styled(Wrapper)`
    height: 100%;
`

const MobileNav = ( {headerHeight, isOpen} ) => {
    const [path, setPath] = useState("/")
    const [targetElement, setTargetElement] = useState()    

    useEffect(() => {
        setPath(window.location.pathname)
    })

    useEffect(() => {
        setTargetElement(document.querySelector('#mobileNav'))
        if (isOpen) {
            disableBodyScroll(targetElement)
        } else {
            enableBodyScroll(targetElement)
        }
    })

    const links = useMainMenu().items.map(item => (
        <MobileNavItem
            to={`/${item.object_slug}`} 
            key={item.title}>
            {item.title}
        </MobileNavItem>
    ))
    const headerHeightString = `${headerHeight}`

    return (
        <MobileNavWrapper
            id="mobileNav"
            headerHeight={headerHeightString}>
            <StyledWrapper>
                <MobileNavInnerWrap>{links}</MobileNavInnerWrap>
            </StyledWrapper>
        </MobileNavWrapper>
    )
}

export default MobileNav
