import React, { useState, useEffect } from "react"
import useForm from 'react-hook-form'
import * as qs from "query-string"
import axios from 'axios'
import Layout from '../components/util/layout'
import Button from '../components/util/button'
import styled from "styled-components"


const FieldWrapper = styled.div`
	position: relative;
	margin-top: 24px;
`

const StyledInput = styled.input`
    display: block;
    padding: 8px 12px;
    width: 100%;
	max-width: 300px;
	
    &:focus + label,
    &:valid + label,
    &:focus ~ label {
        top: -10px;
        color: red;
        font-size: 12px;
        left: 6px;
    }
`

const StyledLabel = styled.label`
    position: absolute;
    top: 50%;
    left: 12px;
    transform: translateY(-50%);
    cursor: text;
	user-select: none;
	pointer-events: none;
    transition: 0.15s ease-in-out;
`

const ContactForm = (props) => {
	const { register, handleSubmit, watch, errors } = useForm()
    const [path, setPath] = useState('/')
    const [message, setMessage] = useState("")
    const allFields = watch()

	useEffect(() => {
		setPath(window.location.pathname)
	})
  
    const onSubmit = async data => {
		const axiosOptions = {
			url: path,
			method: "post",
			headers: { "Content-Type": "application/x-www-form-urlencoded" },
			data: qs.stringify(data),
		}
		let result 
		try {
			result = await axios(axiosOptions)
			console.log(result)
		} catch (errors) {
			console.log(errors)
		}
	}

	
	return (
        <form
            name="contact"
            className={props.className}
            onSubmit={handleSubmit(onSubmit)}
            data-netlify="true"
            data-netlify-honeypot="bot-field"
        >
            <input type="hidden" ref={register} name="bot-field" />
            <input
                type="hidden"
                ref={register({ required: true })}
                name="form-name"
                value="contact"
                required
            />
            <Layout>
                <FieldWrapper itemSize="u-1/2@tablet">
                    <StyledInput
                        name="name"
                        type="text"
                        ref={register}
                        required
                    />
                    <StyledLabel className="label">Your Name</StyledLabel>
                </FieldWrapper>
                <FieldWrapper itemSize="u-1/2@tablet">
                    <StyledInput
                        name="email"
                        type="email"
                        ref={register({ required: true })}
                        required
                    />
                    <StyledLabel>Email</StyledLabel>
                </FieldWrapper>
                <FieldWrapper itemSize="u-1/2@tablet">
                    <StyledInput
                        name="lastname"
                        type="text"
                        ref={register({ required: true })}
                        required
                    />
                    <StyledLabel>lastname</StyledLabel>
                </FieldWrapper>
                <FieldWrapper>
                    <textarea
                        name="message"
                        ref={register({ required: true })}
                    ></textarea>
                </FieldWrapper>
                <Button
                    mod={
                        allFields.email &&
                        allFields.name &&
                        allFields.lastname
                            ? null
                            : "disabled"
                    }
                    className="o-btn--hover-shadow"
                >
                    Send Message
                </Button>
            </Layout>
        </form>
    )
}


export default ContactForm