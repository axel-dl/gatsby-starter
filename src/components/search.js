import React, { useState, useEffect } from "react"
import Axios from "axios"
import * as JsSearch from "js-search"
import { graphql, navigate, useStaticQuery } from "gatsby"
import styled from "styled-components"
import SearchIcon from "../assets/svg/search-solid.inline.svg"

const StyledSearchIcon = styled(SearchIcon)`
    position: absolute;
    right: 14px;
    top: 50%;
    transform: translateY(-50%);
    width: 14px;
`
const SearchBox = styled.input`
    padding: 12px 18px;
    border-radius: 4px 4px 0 0;
    border: 1px solid #f0f0f0;
    width: 100%;
`

const SearchResultsWrap = styled.div`
    display: ${props => props.active ? 'block' : 'none'};
`;

const SearchForm = styled.form`
    position: relative;
`;


const SearchContainer = styled.div`
    max-width: 400px;
    background-color: #f0f0f0;
`

const SearchResult = styled.div`
    padding: 12px;
    border-bottom: 1px solid black;
    position: relative;

    h2 {
        max-width: 300px;
        font-family: ${({ theme }) => theme.fontFamily.body};
    }

    h3 {
        font-size: 12px;
        text-transform: uppercase;
        position: absolute;
        top: 12px;
        right: 12px;
    }
`

const Search = () => {
    const { allWordpressWpSearchResults } = useStaticQuery(graphql`
        {
            allWordpressWpSearchResults {
                edges {
                    node {
                        id
                        post_title
                        searchData
                        post_type
                        post_content
                        post_excerpt
                        pathname
                    }
                }
            }
        }
    `)
    const [allSearchData, setAllSearchData] = useState(
        allWordpressWpSearchResults.edges.map(node => node.node)
    )
    const [search, setSearch] = useState([])
    const [searchResults, setSearchResults] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [searchQuery, setSearchQuery] = useState("")

    useEffect(() => {
        rebuildIndex()
    }, [])

    const rebuildIndex = () => {
        const dataToSearch = new JsSearch.Search("searchData")
        dataToSearch.indexStrategy = new JsSearch.PrefixIndexStrategy()
        dataToSearch.sanitizer = new JsSearch.LowerCaseSanitizer()
        dataToSearch.searchIndex = new JsSearch.TfIdfSearchIndex("searchData")
        dataToSearch.addIndex("post_title")
        dataToSearch.addIndex("post_content")
        dataToSearch.addIndex("searchData")
        dataToSearch.addDocuments(allSearchData)
        setSearch(dataToSearch)
    }

    const searchData = e => {
        const queryResult = search.search(e.target.value)
        setSearchQuery(e.target.value)
        setSearchResults(queryResult)
        console.log(searchQuery, queryResult, allSearchData, searchResults)
    }

    const handleSubmit = e => {
        e.preventDefault()
    }

    const queryResults = searchQuery === "" ? allSearchData : searchResults
    return (
        <SearchContainer>
            <SearchForm onSubmit={handleSubmit}>
                <SearchBox
                    id="Search"
                    value={searchQuery}
                    onChange={searchData}
                    placeholder="Enter your search here"
                />
                <StyledSearchIcon></StyledSearchIcon>
            </SearchForm>
            <SearchResultsWrap active={searchQuery}>
                {queryResults.map(item => {
                    let content
                    if(item.post_excerpt) {
                        content =
                            item.post_excerpt
                                .split(" ")
                                .slice(0, 16)
                                .join(" ") + "..."
                    } else  {
                        content = ""
                    }
                    return (
                        <SearchResult key={item.id}>
                            <h2>{item.post_title}</h2>
                            <h3>{item.post_type}</h3>
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: content,
                                }}
                            />
                        </SearchResult>
                    )
                })}
            </SearchResultsWrap>
        </SearchContainer>
    )
}

export default Search
