import React, { useState, useEffect } from "react"
import styled from "styled-components"

const FilterControlsWrap = styled.div`
    display: flex;
`

const Filter = styled.div`
    padding: 4px 12px;
    border-radius: 3px;
    background-color: ${props => props.active ? "green" : "transparent"};
    border: 1px solid black;
    transition: .2s;
    cursor: pointer;
`
const FilterWrap = styled.div`
    background-color: cornflowerblue;
`

const FilterItemWrap = styled.div`
    background-color: cornflowerblue;
`

const Filters = ({children, filters, allFilterDefault}) => {
    const allFilter = allFilterDefault || 'all'
    const [currentFilter, setCurrentFilter] = useState(allFilter)
    const [filterItems, setFilterItems] = useState(children)

    const cleanFilterName = (string) => string.toLowerCase().replace(" ", "-")
    const sanitizedFilters = filters.map(filter => cleanFilterName(filter))
    const filterControls = sanitizedFilters.map(filterTarget => (
        <Filter
            key={filterTarget}
            active={currentFilter === filterTarget}
            onClick={() => {
                setCurrentFilter(filterTarget)
            }}
            target={[filterTarget, allFilter]}
        >
            {filterTarget}
        </Filter>
    ))

    const handleFilter = (currentFilter, allFilter) => {
        if (currentFilter === allFilter) {
            return children
        } else {
            return React.Children.map(children, (child, index) => {
                if (child.props.filter.includes(currentFilter)) {
                    return (
                        <div
                            key={index}
                        >
                            {child}
                        </div>
                    )
                } else {
                    return null
                }
            })
        }
    }

    useEffect(() => {
        setFilterItems(handleFilter(currentFilter, allFilter))
    }, [currentFilter])


    return (
        <FilterWrap>
            <FilterControlsWrap>
                <Filter
                    active={currentFilter === allFilter}
                    onClick={() => {
                        setCurrentFilter(allFilter)
                    }}
                    target={[allFilter]}
                >
                    {allFilter}
                </Filter>
                {filterControls}
            </FilterControlsWrap>
            <FilterItemWrap>{filterItems}</FilterItemWrap>
        </FilterWrap>
    )
}

export default Filters