import React, { useState, useEffect, useRef } from "react"
import { Link } from "gatsby"
import Logo from "../assets/svg/logo.inline.svg"
import Wrapper from "../components/util/wrapper"
import styled from "styled-components"
import { useMainMenu } from "../hooks/useMainMenu"
import MobileNav from "../components/mobileNav"
import Burger from "../components/util/burger"
import Headroom from "react-headroom"

const StyledHeader = styled.header`
	background-color: #f0f0f0;
	position: fixed;
	top: 0;
	width: 100%;
	transition: 0.4s;
	z-index: 10;
	&.nav-up {
		top: -120px;
	}
`

const BurgerWrap = styled.div`
	display: inline-block;
	width: 36px;
	height: 36px;
`

const HeaderInnerWrap = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    padding: 20px;
    ${({ theme }) => theme.media.lessThan('mobile')`
		background-color: red;
	`};
`

const NavItem = styled(Link)`
	color: #000;
	display: inline-block;
	margin: 8px 16px;
	text-decoration: none;
`

const NavWrap = styled.div`
	display: flex;
`

const StyledLogo = styled(Logo)`
	width: 80px;
`

const MobileNavWrap = styled.div`
	display: ${props => props.isOpen ? "block" : "none"};
`

const Header = () => {
	const [path, setPath] = useState("/")
	const [height, setHeight] = useState(0)
	const [mobileMenuOpen, setMobileMenuOpen] = useState(false)

	useEffect(() => {
		setHeight(document.querySelector('#headerWrap').clientHeight)
	})

	useEffect(() => {
		setPath(window.location.pathname)
	})
	
	const openMobileMenu = () => {
		setMobileMenuOpen(!mobileMenuOpen)
	}

	const links = useMainMenu().items.map((item) => (
		<NavItem to={`/${item.object_slug}`} key={item.title}> 
			{item.title}
		</NavItem>
	))
	
	return (
		<Headroom>
			<StyledHeader id="header">
				<Wrapper>
					<HeaderInnerWrap id="headerWrap">
						<Link to="/">
							<StyledLogo></StyledLogo>
						</Link>
						<NavWrap>
							{links}
							<BurgerWrap>
								<Burger onClick={() => openMobileMenu()}></Burger>
							</BurgerWrap>
						</NavWrap>
					</HeaderInnerWrap>
				</Wrapper>
				<MobileNavWrap
					isOpen={mobileMenuOpen}
				>
					<MobileNav 
						isOpen={mobileMenuOpen} 
						headerHeight={height}></MobileNav>
				</MobileNavWrap>
			</StyledHeader>
		</Headroom>
    )
}

export default Header
