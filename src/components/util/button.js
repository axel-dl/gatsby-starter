import React from "react"
import "../../styles/all.scss"

const Button = ({ mod, children, className, onClick, disabled }) => (
    <button
        disabled={disabled}
        onClick={onClick}
        className={`o-btn o-btn--${mod} ${className}`}
    >
        {children}
    </button>
)

export default Button
