import React from "react";
import '../../styles/all.scss'

const Layout = ({children, size, itemSize, moduleOption, className, ...props}) => {
    const moduleClass = moduleOption ? 'o-module__item' : ''
    const layoutItems = React.Children.map(children , (child, index) => {
        const defaultItemSize = itemSize === undefined ? '' : itemSize
        return (
            <div
                key={index}
                className={`o-layout__item ${
                    child.props.itemSize === undefined
                        ? defaultItemSize
                        : child.props.itemSize
                } ${moduleClass === undefined ? "" : moduleClass}`}
            >
                {child}
            </div>
        )
    })
    
    return (
        <section className={
            `o-layout 
            ${size ? `o-layout--${size}` : ''} 
            ${moduleOption ? `o-module` : ''}
            ${className}`
        }>
            {layoutItems}
        </section>
    )
}

export default Layout