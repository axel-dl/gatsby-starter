import React from "react"
import styled from 'styled-components'
import { Link, graphql, useStaticQuery } from 'gatsby'
import Wrapper from '../components/util/wrapper'
import Logo from "../assets/svg/logo.inline.svg"
import Img from "gatsby-image"
// import QueryTest from './helpers/fragments'
import InstagramGallery from './instagramGallery'
import BackgroundImage from "gatsby-background-image"

const CopySection = styled.div`
	a 	{
		opacity: .65;
		text-decoration: none;
	}
`
const FooterInnerWrap = styled.footer`
	display: flex;
	justify-content: space-between;
	width: 100%;
`

const StyledLogo = styled(Logo)`
    width: 100px;
`

const Footer = () => {
	const Copyright = styled.div`
		color: ${props => props.theme.colors.primary};
    `
    const data = useStaticQuery(graphql`
            {
            allWordpressSiteMetadata {
                edges {
                    node {
                        name
                    }
                }
            }
            allWordpressPost {
                edges {
                    node {
                        id
                        featured_media {
                            localFile {
                                childImageSharp {
                                    fluid(quality: 90, maxWidth: 1920) {
                                        ...GatsbyImageSharpFluid_withWebp
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    `)
    const date = new Date()
    const footerContext = data.allWordpressSiteMetadata.edges[0].node
    const imgData =
        data.allWordpressPost.edges[0].node.featured_media
            .localFile.childImageSharp.fluid
    return (
        <footer>
            <InstagramGallery></InstagramGallery>
            <Wrapper>
                <FooterInnerWrap>
                    <StyledLogo></StyledLogo>
                    <CopySection>
                        <h1>Footer here</h1>
                        <Img fluid={imgData} />
                        <BackgroundImage
                            Tag="section"
                            fluid={imgData}
                            backgroundColor={`#040e18`}
                        >
                            <h2>Background Image</h2>
                        </BackgroundImage>
                        <p>
                            <Link to={"/terms-conditions/"}>
                                Terms&nbsp;&amp;&nbsp;Conditions
                            </Link>
                            &nbsp;|&nbsp;
                            <Link to={"/privacy-policy/"}>
                                Privacy
                            </Link>
                            &nbsp;|&nbsp;Built&nbsp;by&nbsp;
                            <a
                                target="_blank"
                                href="https://rockagency.com.au/"
                            >
                                Rock&nbsp;Agency
                            </a>
                        </p>
                        <Copyright>
                            <p>
                                &copy;&nbsp;Copyright&nbsp;
                                {footerContext.name}&nbsp;
                                {date.getFullYear()}.
                            </p>
                        </Copyright>
                    </CopySection>
                </FooterInnerWrap>
            </Wrapper>
        </footer>
    )
}

export default Footer	
