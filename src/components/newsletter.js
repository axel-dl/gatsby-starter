import React, { useState, useEffect } from "react"
import useForm from "react-hook-form"
import * as qs from "query-string"
import axios from "axios"
import Button from "../components/util/button"
import styled from "styled-components"


const StyledInput = styled.input`
    display: inline-block;
    padding: 8px 12px;
    width: 100%;
    max-width: 300px;
`


const Newsletter = (props) => {
    const { register, handleSubmit, watch, errors } = useForm()
    const [path, setPath] = useState("/")
    const [message, setMessage] = useState("")
    const allFields = watch()

    useEffect(() => {
        setPath(window.location.pathname)
    })

    const onSubmit = async data => {
        const axiosOptions = {
            url: path,
            method: "post",
            headers: { "Content-Type": "application/x-www-form-urlencoded" },
            data: qs.stringify(data),
        }
        let result
        try {
            result = await axios(axiosOptions)
            console.log(result)
        } catch (errors) {
            console.log(errors)
        }
    }

    return (
        <form
            name="newsletter"
            className={props.className}
            onSubmit={handleSubmit(onSubmit)}
            data-netlify="true"
            data-netlify-honeypot="bot-field"
        >
            <input type="hidden" ref={register} name="bot-field" />
            <input
                type="hidden"
                ref={register({ required: true })}
                name="form-name"
                value="newsletter"
                required
            />
            <StyledInput
                name="email"
                type="email"
                placeholder="Your Email"
                ref={register({ required: true })}
                required
            />
            <Button mod={allFields.email ? null : "disabled"}>Signup</Button>
        </form>
    )
}

export default Newsletter
