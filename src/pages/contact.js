import {
    graphql,
    useStaticQuery
} from "gatsby"
import styled from "styled-components"
import React from "react"
import SiteWrapper from "../components/util/siteWrapper"
import ContactForm from "../components/contactForm"
import SEO from '../components/seo'
import Wrapper from "../components/util/wrapper"

export default () => {
    const data = useStaticQuery(graphql `
        {
            allWordpressPage(filter: {slug: {eq: "contact"}}) {
                edges {
                    node {
                        content
                        title
                        yoast_meta {
                            yoast_wpseo_metadesc
                            yoast_wpseo_title
                        }
                    }
                }
            }
        }`)
    const pageContext = data.allWordpressPage.edges[0].node
    return (
        <SiteWrapper>
            <SEO
                title={pageContext.yoast_meta.yoast_wpseo_title}
                description={pageContext.yoast_meta.yoast_wpseo_metadesc}
            />
            <Wrapper>
                <h1 dangerouslySetInnerHTML={{ __html: pageContext.title }} />
                <div dangerouslySetInnerHTML={{ __html: pageContext.content }} />
                <ContactForm></ContactForm>
            </Wrapper>
        </SiteWrapper>
    )
}