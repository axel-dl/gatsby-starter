import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import SEO from "../components/seo"
import SiteWrapper from "../components/util/siteWrapper"
import Search from "../components/search"
import ContactForm from "../components/contactForm"
import GoogleMap  from "../components/googleMap"
import Filters from "../components/filters"
import Newsletter from "../components/newsletter"
import Select from "react-select"

export default () => {
	const options = [
        { value: "chocolate", label: "Chocolate" },
        { value: "strawberry", label: "Strawberry" },
        { value: "vanilla", label: "Vanilla" },
    ]
	const { allWordpressWpFrontpage } = useStaticQuery(graphql`
        {
            allWordpressWpFrontpage {
				nodes {
					id
					slug
					status
					template
					title
					content
					yoast_meta {
						yoast_wpseo_metadesc
						yoast_wpseo_title
					}
				}
            }
        }
    `)
	const home = allWordpressWpFrontpage.nodes[0]
	return (
        <SiteWrapper>
            <SEO title={home.title} description={home.description} />
            <h1>{home.title}</h1>
            <div dangerouslySetInnerHTML={{ __html: home.content }} />
            <Filters filters={["red", "blue", "green"]}>
                <p filter={["red"]}>Red</p>
                <p filter={["blue"]}>Blue</p>
                <p filter={["red"]}>REd</p>
                <p filter={["green"]}>Green</p>
                <p filter={["green"]}>Green</p>
                <p filter={["green", "red"]}>Green & Red</p>
            </Filters>
            <Newsletter></Newsletter>
            <Search></Search>
            <div style={{ height: "100vh", width: "100%" }}>
                <GoogleMap></GoogleMap>
            </div>
            <Search></Search>
            <Select options={options} />
            <ContactForm></ContactForm>
        </SiteWrapper>
    )
}
