import { StaticQuery, graphql, navigate, useStaticQuery } from "gatsby";
import styled from "styled-components";
import React from "react"
import SiteWrapper from "../components/util/siteWrapper"
import Search from "../components/search"

export default () => (
        <SiteWrapper>
            <StaticQuery
                query={graphql`
                    {
                        allWordpressWpSearchResults {
                            edges {
                                node {
                                    id
                                    post_title
                                    searchData
                                    post_type
                                    post_content
                                    pathname
                                }
                            }
                        }
                    }
                `}
                render={data => {
                    return (
                        <Search
                            data={data}
                            minCharacters={4}
                            contentCharacters={300}
                            maxResults={10}
                            placeholder="Search"
                            onSelect={object => navigate(o.pathname)}
                        />
                    )
                }}
            />
        </SiteWrapper>
    )