import React from 'react'
import Wrapper from '../components/util/wrapper'
import Layout from '../components/util/layout'
import SiteWrapper from '../components/util/siteWrapper'

const NotFoundPage = () =>  {
    
    return (
        <SiteWrapper>
            <Wrapper>
                <Layout moduleOption itemSize="u-1/1">
                    <div>
                        <h1>NOT FOUND</h1>
                        <p>
                            Sorry this page does not exist.
                        </p>
                    </div>
                </Layout>
            </Wrapper>
        </SiteWrapper>
    )
}

export default NotFoundPage
