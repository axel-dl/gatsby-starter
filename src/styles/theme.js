import { generateMedia } from "styled-media-query"
const colors = {
    primary: "#201847",
    secondary: "#f9c146",
    tertiary: "#6a6a6a",
    content: "#f0f0f0",
}

const media = generateMedia({
    mobile: "375px",
	mobileLandscape: "600px",
	tablet: "770px",
	tabletWide: "1000px",
	laptop: "1280px",
	desktop: "1440px",
	wideScreen: "1600px",
})



const fontSizes = {
    body: "16px"
}

const fontFamily = {
    body: "Raleway Regular",
    heading: "sans serif",
    link: "sans serif"
}

const theme = {
    colors,
    media,
    fontSizes,
    fontFamily
}

export default theme