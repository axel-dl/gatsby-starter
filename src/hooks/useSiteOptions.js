// * this is an example of how to use a hook for the options page
// import { useStaticQuery, graphql } from "gatsby"


// const useSiteOptions = () => {
//     const { allWordpressAcfOptions } = useStaticQuery(
//         graphql`
//             {
//                 allWordpressAcfOptions {
//                     edges {
//                         node {
//                             options_page_1 {
//                                 email
//                                 address
//                                 facebook
//                                 instagram
//                                 linkedin
//                                 phone
//                                 twitter
//                             }
//                         }
//                     }
//                 }
//             }
//         `
//     )
//     return allWordpressAcfOptions.edges[0].node.options_page_1
// }

// export default useSiteOptions
