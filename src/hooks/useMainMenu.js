import { useStaticQuery, graphql } from "gatsby"
export const useMainMenu = () => {
    const { allWordpressWpApiMenusMenusItems } = useStaticQuery(
        graphql`
            {
                allWordpressWpApiMenusMenusItems(
                    filter: { name: { eq: "Main" } }
                ) {
                    edges {
                        node {
                            name
                            items {
                                title
                                object_slug
                            }
                        }
                    }
                }
            }
        `
    )
    return allWordpressWpApiMenusMenusItems.edges[0].node
}
