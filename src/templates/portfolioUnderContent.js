import React from "react"
import SEO from "../components/seo"
import SiteWrapper from "../components/util/siteWrapper"

export default ({ pageContext }) => {
    return (
        <SiteWrapper>
            <SEO
                title={pageContext.yoast_meta.yoast_wpseo_title}
                description={pageContext.yoast_meta.yoast_wpseo_metadesc}
            />
            <h1 dangerouslySetInnerHTML={{ __html: pageContext.title }} />
            <div dangerouslySetInnerHTML={{ __html: pageContext.content }} />
        </SiteWrapper>
    )
}
