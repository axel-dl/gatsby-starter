import React from "react"
import Layout from "../components/util/layout"
import { graphql } from "gatsby"

export default ({ pageContext }) => {
	return (
		<Layout>
			<div>
				<h1>{pageContext.title}</h1>
				<div dangerouslySetInnerHTML={{ __html: pageContext.content }} />
			</div>
		</Layout>
	)
}
