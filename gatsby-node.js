const _ = require("lodash")
const path = require("path")
const slash = require(`slash`)
const { createFilePath } = require("gatsby-source-filesystem")
const { paginate } = require("gatsby-awesome-pagination")

const toCamelCase = s => {
    return s.replace(/([-_][a-z])/gi, $1 => {
        return $1
            .toUpperCase()
            .replace("-", "")
            .replace("_", "")
    })
}
const getOnlyPublished = edges =>
    _.filter(edges, ({ node }) => node.status === "publish")
    
const formatTemplate = string => string.slice(0, -4).toLowerCase()

const pagesToSkipCreation = [
    "home",
    "contact"
]

exports.createPages = async ({ actions, graphql }) => {
    const { createPage, createRedirect } = actions
    // Redirecting to home page
    createRedirect({
        fromPath: "/home",
        toPath: "/",
        redirectInBrowser: true,
        isPermanent: true,
    })
    try {
        // 	========= PAGES ============
        const pagesData = await graphql(
            `
                {
                    allWordpressPage {
                        edges {
                            node {
                                id
                                slug
                                status
                                template
                                title
                                content
                                wp_path: path
                                yoast_meta {
                                    yoast_wpseo_metadesc
                                    yoast_wpseo_title
                                }
                            }
                        }
                    }
                }
            `
        )

        const allPages = pagesData.data.allWordpressPage.edges
        const pages =
            process.env.NODE_ENV === "production"
                ? getOnlyPublished(allPages)
                : allPages

        // Call `createPage()` once per WordPress page
        // Each page is required to have a `path` as well
        // as a template component. The `context` is
        // optional but is often necessary so the template
        // can query data specific to each page.
        _.each(pages, ({ node: page }) => {
            if (pagesToSkipCreation.includes(page.slug)) {
                return
            }
            const pageTemplate = page.template
                ? path.resolve(`./src/templates/${toCamelCase(formatTemplate(page.template))}.js`)
                : path.resolve("./src/templates/page.js")
            console.log(`CREATING PAGE - ${page.slug}`)
            createPage({
                path: `${page.wp_path}`,
                component: pageTemplate,
                context: page,
            })
        })
        // 	========= END PAGES ========

        // 	========= POSTS ============
        const postsData = await graphql(`
            {
                allWordpressPost {
                    edges {
                        node {
                            title
                            content
                            excerpt
                            template
                            status
                            date(formatString: "Do MMM YYYY HH:mm")
                            wordpress_id
                            slug
                            wp_path: path
                            yoast_meta {
                                yoast_wpseo_metadesc
                                yoast_wpseo_title
                            }
                        }
                    }
                }
            }
        `)

        const blogTemplate = path.resolve(`./src/templates/blog.js`)

        // In production builds, filter for only published posts.
        const allPosts = postsData.data.allWordpressPost.edges
        const posts =
            process.env.NODE_ENV === "production"
                ? getOnlyPublished(allPosts)
                : allPosts

        // Iterate over the array of posts
        _.each(posts, ({ node: post }) => {
            const postTemplate = post.template
                ? path.resolve(`./src/templates/${toCamelCase(formatTemplate(post.template))}.js`)
                : path.resolve("./src/templates/post.js")
            console.log(`CREATING POST - ${post.slug}`)
            // Create the Gatsby page for this WordPress post
            createPage({
                path: `${post.wp_path}`,
                component: postTemplate,
                context: post,
            })
        })
        // 	========= END POSTS ========
    } catch (error) {
        console.log(error)
    }
}
