// For Robots.txt plugin and netlify
const {
    NODE_ENV,
    URL: NETLIFY_SITE_URL = "https://www.example.com",
    DEPLOY_PRIME_URL: NETLIFY_DEPLOY_URL = NETLIFY_SITE_URL,
    CONTEXT: NETLIFY_ENV = NODE_ENV,
} = process.env
const isNetlifyProduction = NETLIFY_ENV === "production"
const siteUrl = isNetlifyProduction ? NETLIFY_SITE_URL : NETLIFY_DEPLOY_URL

require("dotenv").config({
    path: ".env",
})

module.exports = {
    siteMetadata: {
        title: `Rockagency Starter`,
        description: `Base template for Rockagency`,
        author: `@gatsbyjs`,
        image: `/assets/image/favicon.png`,
        siteUrl: siteUrl,
    },
    plugins: [
        `gatsby-plugin-react-helmet`,
        `gatsby-plugin-sass`,
        `gatsby-plugin-styled-components`,
        `gatsby-plugin-remove-trailing-slashes`,
        `gatsby-plugin-catch-links`,
        // `gatsby-plugin-transition-link`,
        {
            resolve: `gatsby-plugin-favicon`,
            options: {
                logo: "./src/assets/images/favicon.png",
            },
        },
        {
            resolve: "gatsby-plugin-web-font-loader",
            options: {
                custom: {
                    families: ["Raleway Regular"],
                    urls: ["./src/styles/settings/fonts.scss"],
                },
            },
        },
        {
            resolve: `gatsby-source-instagram`,
            options: {
                username: `rockagencysocial`,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/assets/images`,
            },
        },
        {
            resolve: "gatsby-source-wordpress",
            options: {
                /*
                 * The base URL of the Wordpress site without the trailingslash and the protocol. This is required.
                 * Example : `gatsbyjsexamplewordpress.wordpress.com` or `www.example-site.com`
                 */
                baseUrl: process.env.API_URL,
                // The protocol. This can be http or https.
                protocol: process.env.API_PROTOCOL,
                // Indicates whether the site is hosted on wordpress.com.
                // If false, then the assumption is made that the site is self hosted.
                // If true, then the plugin will source its content on wordpress.com using the JSON REST API V2.
                // If your site is hosted on wordpress.org, then set this to false.
                hostingWPCOM: false,
                // If useACF is true, then the source plugin will try to import the Wordpress ACF Plugin contents.
                // This feature is untested for sites hosted on wordpress.com.
                // Defaults to true.
                useACF: true,
                // Include specific ACF Option Pages that have a set post ID
                // Regardless if an ID is set, the default options route will still be retrieved
                // Must be using V3 of ACF to REST to include these routes
                // Example: `["option_page_1", "option_page_2"]` will include the proper ACF option
                // routes with the ID option_page_1 and option_page_2
                // The IDs provided to this array should correspond to the `post_id` value when defining your
                // options page using the provided `acf_add_options_page` method, in your WordPress setup
                // Dashes in IDs will be converted to underscores for use in GraphQL
                acfOptionPageIds: [],
                auth: {
                    // If auth.user and auth.pass are filled, then the source plugin will be allowed
                    // to access endpoints that are protected with .htaccess.
                    htaccess_user: "your-htaccess-username",
                    htaccess_pass: "your-htaccess-password",
                    htaccess_sendImmediately: false,

                    // If you use "JWT Authentication for WP REST API" (https://wordpress.org/plugins/jwt-authentication-for-wp-rest-api/)
                    // or (https://github.com/jonathan-dejong/simple-jwt-authentication) requires jwt_base_path, path can be found in wordpress wp-api.
                    // plugin, you can specify user and password to obtain access token and use authenticated requests against wordpress REST API.
                    jwt_user: process.env.JWT_USER,
                    jwt_pass: process.env.JWT_PASSWORD,
                    jwt_base_path: "/jwt-auth/v1/token", // Default - can skip if you are using https://wordpress.org/plugins/jwt-authentication-for-wp-rest-api/
                },
                // Set cookies that should be send with requests to wordpress as key value pairs
                cookies: {},
                // Set verboseOutput to true to display a verbose output on `npm run develop` or `npm run build`
                // It can help you debug specific API Endpoints problems.
                verboseOutput: false,
                // Set how many pages are retrieved per API request.
                perPage: 100,
                // Search and Replace Urls across WordPress content.
                searchAndReplaceContentUrls: {
                    sourceUrl: "https://source-url.com",
                    replacementUrl: "https://replacement-url.com",
                },
                // Set how many simultaneous requests are sent at once.
                concurrentRequests: 10,
                // Set WP REST API routes whitelists
                // and blacklists using glob patterns.
                // Defaults to whitelist the routes shown
                // in the example below.
                // See: https://github.com/isaacs/minimatch
                // Example:  `["/*/*/comments", "/yoast/**"]`
                // ` will either include or exclude routes ending in `comments` and
                // all routes that begin with `yoast` from fetch.
                // Whitelisted routes using glob patterns
                includedRoutes: [
                    "**/categories",
                    "**/posts",
                    "**/pages",
                    "**/media",
                    "**/tags",
                    "**/taxonomies",
                    "**/frontpage",
                    "**/searchResults",
                    // "**/users",
                    "**/menus",
                    // "**/portfolio",
                    // Add CPT here
                    "**/logo",
                    "**/favicon",
                ],
                // Blacklisted routes using glob patterns
                excludedRoutes: [],
                // use a custom normalizer which is applied after the built-in ones.
                normalizer: function({ entities }) {
                    return entities
                },
            },
        },
        `gatsby-plugin-sitemap`,
        `gatsby-plugin-sharp`,
        `gatsby-transformer-sharp`,
        {
            resolve: "gatsby-plugin-robots-txt",
            options: {
                resolveEnv: () => NETLIFY_ENV,
                env: {
                    production: {
                        policy: [
                            {
                                userAgent: "*",
                            },
                        ],
                    },
                    "branch-deploy": {
                        policy: [
                            {
                                userAgent: "*",
                                disallow: ["/"],
                            },
                        ],
                        sitemap: null,
                        host: null,
                    },
                    "deploy-preview": {
                        policy: [
                            {
                                userAgent: "*",
                                disallow: ["/"],
                            },
                        ],
                        sitemap: null,
                        host: null,
                    },
                },
            },
        },
        {
            resolve: "gatsby-plugin-react-svg",
            options: {
                rule: {
                    include: /\.inline\.svg$/,
                },
            },
        },
        // {
        //     // Removes unused css rules
        //     //! doesn't seem to be working so commenting out for the moment
        //     resolve: `gatsby-plugin-purgecss`,
        //     options: {
        //         // Activates purging in gatsby develop
        //         develop: true,
        //         // Purge only the main css file
        //         purgeOnly: [`../styles/all.scss`],
        //     },
        // }, // must be after other CSS plugins
        {
            resolve: `gatsby-plugin-google-analytics`,
            options: {
                trackingId: "UA-149584216-3",
                head: false,
            },
        },
        `gatsby-plugin-netlify`, // make sure to keep it last in the array
    ],
}
